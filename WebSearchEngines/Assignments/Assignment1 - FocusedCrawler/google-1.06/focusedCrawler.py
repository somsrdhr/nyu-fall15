
from google import search
from urlparse import urlparse
from collections import defaultdict
import urllib, htmllib, formatter
import heapq
import re
import time
import sys
import csv
import robotparser

''' htmlParser - To extract the links from the crawled page as shown by the guidelines/tips for this assignment.
.   Extra condition added at line 25 to return only http links and not null links. 
'''
class LinksExtractor(htmllib.HTMLParser):
    def __init__(self, formatter):      
        htmllib.HTMLParser.__init__(self, formatter)  
        self.links = []        

    def start_a(self, attrs):  
        if len(attrs) > 0:
            for attr in attrs:
                if attr[0] == "href":
                	if "http" in attr[1]:
                		self.links.append(attr[1])

    def get_links(self):    
        return self.links

#To take care of password proteted pages as shown by the guidlines/tips for this assignmnet
class myURLopener(urllib.FancyURLopener):

    def http_error_401(self, url, fp, errcode, errmsg, headers, data=None):
        return None	# do nothing

''' Function to calculate priority for URL 
.   The priority is based on the Priority seedUrlPriority, numberOfLinks and wordCountUrl.
.   seedUrlPriority :  is the pririty of the parent link 
.   numberOfLinks : the number of Outlinks for that particular page.
.   wordCountUrl : the number of occurences of the query string in the url itself.
'''
def calculatePriority(url, seedUrlPriority, numberOfLinks):
	splitStrings= searchQuery.split(' ')
	wordCountUrl = url.count(searchQuery)
	urlDictionary[url][4] = wordCountUrl
	priority = seedUrlPriority/numberOfLinks - (1000*wordCountUrl)
	return priority

''' Function to check if the crawler is allowed to parse a particular url.
.   Implemented using 'robotparser'
'''
def canParse(url):
	rp = robotparser.RobotFileParser()
	urlParseResult = urlparse(url)
	urlParsed = urlParseResult[0] + "://" + urlParseResult[1] + "/robots.txt"
	rp.set_url(urlParsed)
	rp.read()
	return rp.can_fetch("*", url)


startTime = time.time()			#To calculate Runtime
'''
. urlPriorityQueue : Priority queue which stores the url and the priority. 
. urlDictionary: Is a dictionary of the links that are crawled that maps the url to various parameters 
. urlDictionary[url][0]  		Represents the priority 		
. urlDictionary[url][1]			Incoming links for URL
. urlDictionary[url][2] 		Outgoing links for URL
. urlDictionary[url][3]			Query Word Count for URL
. urlDictionary[url][4] 		Query word in the URL itself
. urlDictionary[url][5] 		Page Size for URL				
. urlDictionary[url][6] 		Depth of the URL from the Seed pages
'''
urlPriorityQueue = [] 			
urlDictionary = {}				
counter = 1						#Counter to keep track of pages
i = 0							#General counter to loop through
sumTotalPages = 0				#to calculate the total size of pages downloaded
cantParsePages = 0				#keep track of pages that couldn't be parsed by the robot.txt

'''Getting the search results from Google Search Engine. The top ten results are considered as seedUrl's
.  Set high priority to these pages and push into Priority Queue 'urlPriorityQueue'
'''
searchQuery = sys.argv[1]
searchResults = search(searchQuery, stop =10)

''' All the required data/statiscs is written into a csv file.
'''
with open(sys.argv[3], 'w') as fp:
    a = csv.writer(fp, delimiter=',')
    data = [["URL", "Priority", "Outgoing Links", "Incoming Links", "Depth", "Word Count for page"," Word Count(Url)", "Page Size"]]
    a.writerows(data)
	
''' In this for loop, the initial values for the top seed urls are set such as priority, number of incoming links, 
.	word count of the search query in the url.
.   The ten seed urls are added to the priority queue
'''
for url in searchResults:
	if(i<10):
		parsedResult = urlparse(url)
		url= "https://" + parsedResult[1] + parsedResult[2]
		value = -100000 + (i*10);										#Setting priority
		urlDictionary[url] = []
		urlDictionary[url] = [int(value),0,0,0,0,0,1]						
		heapq.heappush(urlPriorityQueue, (urlDictionary[url][0], url))	
		wordCountUrl = url.count(searchQuery)							
		urlDictionary[url][4] = wordCountUrl							#Setting word count of the search query in the url.
		i += 1

''' Main while loop to start the crawler.
.   Counter keeps track of the pages downloaded/parsed
.   At each pass, seedUrl is the url popped from the priority queue.
'''
while(counter < int(sys.argv[2])):
	print "\nCounter:" + str(counter)
	#print "Priority Queue size:" + str(len(urlPriorityQueue))
	seedUrl = heapq.heappop(urlPriorityQueue)
	seedUrlName = seedUrl[1]
	seedUrlPriority = seedUrl[0]
	#print "Url" + seedUrlName 
	#print "Depth" + str(urlDictionary[seedUrlName][6])
	
	try:
		format = formatter.NullFormatter()         						# create default formatter
		htmlparser = LinksExtractor(format)        						# create new parser object
		urlOpener = myURLopener()			 							# create URLopener
		data = urlOpener.open(seedUrlName)   							# open file by url
		fileContent = data.read()					
		urlDictionary[seedUrlName][5] = len(fileContent)				# Storing the file size in Bytes
		urlDictionary[seedUrlName][3] = fileContent.find(searchQuery)	# Number of occurences of search query in the downloaded page
		htmlparser.feed(fileContent)      								# parse the file saving the info about links
		htmlparser.close()
		links = htmlparser.get_links()   								# get the hyperlinks list
		urlDictionary[seedUrlName][2] = len(links)						# Storing the number of outgoing links
	except (IOError, htmllib.HTMLParseError), error:
		links = ''
		print 'IOError' 


	''' The hyperlinks obtained are further checked for duplicates, if it can be parsed, etc..
		Duplicate links are updated accordingly. The priority is cummulative and depends on the priority of the new parent.
		new priority = { (cummulativeOldPririoty * number of incoming links) + priority of the current page based on new parent}
						---------------------------------------------------------------------------------------------------------
														number of incoming links + 1
	    The number of incoming links for the duplicate link is updated.

	    If the link is not duplicate, the priority is calculated as in the calculatePriority function.
	    Other paramaters are set for the new url and pushed into the priority queue.
	'''		

	#print 'Scanning through hyperlinks...'				
	for urlName in links:
		parsedResult = urlparse(urlName)
		urlName = "https://" + parsedResult[1] + parsedResult[2]
		#print urlName   
		if urlName in urlDictionary:
			priority = seedUrlPriority/len(links) - (1000*urlDictionary[urlName][4])	
			cummulativePriority = (urlDictionary[urlName][0]*urlDictionary[urlName][1] + priority) / (urlDictionary[urlName][1] + 1)
			urlDictionary[urlName][1] += 1
		else:
			if(canParse(url)):
				urlDictionary[urlName] =  [0,0,0,0,0,0,0]											# initialize parameters
				urlDictionary[urlName][0] = calculatePriority(urlName, seedUrlPriority, len(links))	# setpriority														#
				urlDictionary[urlName][6] = urlDictionary[seedUrlName][6] + 1						# depth of page = depth of page + 1
				heapq.heappush(urlPriorityQueue, (urlDictionary[urlName][0], urlName))				# push into queue
			else:
				cantParsePages += 1	
	with open(sys.argv[3], 'a') as fp:																# Write into csv file
		a = csv.writer(fp, delimiter=',')
		data = [[seedUrlName, urlDictionary[seedUrlName][0]*(-1), urlDictionary[seedUrlName][2], urlDictionary[seedUrlName][1], urlDictionary[seedUrlName][6], urlDictionary[seedUrlName][3],urlDictionary[seedUrlName][4], urlDictionary[seedUrlName][5]]]
		a.writerows(data)
	sumTotalPages = sumTotalPages + urlDictionary[seedUrlName][5]									# To get total size of data downloaded	
	counter += 1

''' Various statiscs are calculated
'''

totalNumberCrawled = len(urlPriorityQueue) + int(sys.argv[2]) 			# Total number of pages based on remainning links in priority queue and N							
avgPageSize = sumTotalPages/ int(sys.argv[2])										
runTime = time.time() - startTime										# Run time for focused Crawler

with open(sys.argv[3], 'a') as fp:
	a = csv.writer(fp, delimiter=',')
	data = [["Total time taken", runTime]]
	a.writerows(data)	
	data = [["Total size of the pages downloaded", sumTotalPages]]
	a.writerows(data)
	data = [["Avg size of the pages downloaded", avgPageSize]]
	a.writerows(data)
	data = [["Total number of files crawled", totalNumberCrawled]]
	a.writerows(data)

print '\n\n\n'
print 'Parsing Completed, check '+ sys.argv[3] +' for statistics'
print 'URLS and other details saved to ' + sys.argv[3]
print 'Time taken to complete: ' + str(runTime)

