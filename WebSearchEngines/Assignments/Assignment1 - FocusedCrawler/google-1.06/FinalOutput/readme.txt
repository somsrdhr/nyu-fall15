Files submitted
1. focusedCrawler.py - Main python module 
2. readme.txt - ReadMe file 
3. Output files - focusedCralwerOutput1.csv : Search query was 'web' for N = 500 
				  focusedCralwerOutput2.csv : Search query was 'books' for N = 500
				  focusedCralwerOutput3.csv : Search query was 'cat' for N = 100
				  focusedCralwerOutput4.csv : Search query was 'harry' for N = 50 

4. explain.txt - Explainning the basic runthrough and bugs.

Other Files
google.py - Module to get the search query
SEt up files for those - ex_Setup.py, MANIFEST.in, PKG-INFO, README.md, requirements.txt, setup.py, setuptools-18.3.1.zip

Sample command line arguments

Example : python focusedCrawler.py books 500 Output.csv
There are three arguments

1. books - the search query
2. 500 - the number of pages to be crawled
3. output.csv - the output file which will contain all the statistics

Restrcitions on the input query - For now, should contain spaces.


